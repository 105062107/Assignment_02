# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
    * 


* **玩法**
    * 用上下左右鍵控制
    * 開始頁面有3個選項，可以先輸入名子，記分板中就會有名子的資料
    * 中下兩個選項分別是1P跟2P，規則就是小朋友下樓梯
    * 結束時可以重新或是回到原本的畫面
    * UI是輸入名子的頁面還有遊戲中的瞬移按鈕

* **分數**
    * A complete game process: start menu => game view => game over => quit or play again 有達成 
    * Your game should follow the basic rules of  "小朋友下樓梯".  有達成
    *  All things in your game should have correct physical properties and behaviors. 有達成
    * Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform)有達成
    * Add some additional sound effects and UI to enrich your game.有達成
    * Store player's name and score in firebase real-time database, and add a leaderboard to your game.有達成


* **Other creative features in your game:**
    * 雙人下樓梯有踩人機制 
    * 瞬移按鈕
    * ★★★★★請在開始頁面狂按下鍵★★★★★

就會出現隱藏模式(SUPER HARD MODE)
隱藏關卡有分成6關
第一關是全部正常階梯
第二關是全部會碎裂的階梯
第三關會直接把血量最大上限改到4，再稍微變動地板出現機率
第四關會只有正常階梯跟彈跳階梯，但彈跳階梯會跳很高，很容易撞到天花板
第五關階梯距離會被拉長而且沒有回血階梯
第六關速度會變爆幹快

過關方法:
第一關要結束時站在最上方，才能幾乎剛好過第二關
第四關則是要保持在下方