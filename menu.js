var menu_num = 1;
var stmeu;
var selectSound;
var presstimes;
var startLabel_HARDMODE;
var haveset = 0;
var bombSound;
var menuState = {

    preload: function() {
        game.load.image('startmenu', 'assets/start.png');
        game.load.audio('select', ['assets/select.wav', 'assets/select.ogg']);
        game.load.audio('jump', ['assets/jump.wav', 'assets/jump.ogg']);
        game.load.audio('hurt', ['assets/hurt.wav', 'assets/hurt.ogg']);
        game.load.audio('step', ['assets/step.wav', 'assets/step.ogg']);
        game.load.audio('gg', ['assets/gg.wav', 'assets/gg.ogg']);
        game.load.audio('bomb', ['assets/bomb.wav', 'assets/bomb.ogg']);

        game.load.image('buttonleft', 'assets/button.png');
        game.load.image('buttonright', 'assets/button(1).png');
        game.load.image('buttonup', 'assets/button(2).png');
    },

    create: function() {

        selectSound = game.add.audio('select');
        bombSound = game.add.audio('bomb');
        
        menu_num = 1;
        presstimes = 0;
        haveset = 0;
        stmeu = game.add.sprite(100, 300, 'startmenu');

        var nameLabel = game.add.text(game.width/2, 80, '小朋友下樓梯',
        { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        var startLabel_1P = game.add.text(game.width/2, game.height-280,    
        '1P', { font: '25px Arial', fill: '#ffffff' });
        startLabel_1P.anchor.setTo(0.5, 0.5);

        var startLabel_2P = game.add.text(game.width/2, game.height-200,    
        '2P', { font: '25px Arial', fill: '#ffffff' });
        startLabel_2P.anchor.setTo(0.5, 0.5);

        var startLabel_ENTERNAME = game.add.text(game.width/2, game.height-120,    
        'Enter Name', { font: '25px Arial', fill: '#ffffff' });
        startLabel_ENTERNAME.anchor.setTo(0.5, 0.5);

        startLabel_HARDMODE = game.add.text(game.width/2, game.height-40,    
        'SUPER HARD MODE', { font: '25px Arial', fill: '#ffffff' });
        startLabel_HARDMODE.anchor.setTo(0.5, 0.5);
        
        startLabel_HARDMODE.visible = false;

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        upKey.onDown.add(this.up, this);
        downKey.onDown.add(this.down, this);
        enterKey.onDown.add(this.enter, this);

    },

    up: function() {
        presstimes = 0;
        selectSound.play();
        if(menu_num != 1) menu_num--;
        stmeu.kill();
        if(menu_num == 1) stmeu = game.add.sprite(100, 300, 'startmenu');
        if(menu_num == 2) stmeu = game.add.sprite(100, 380, 'startmenu');
        if(menu_num == 3) stmeu = game.add.sprite(100, 460, 'startmenu');
    },

    down: function() {
        if(presstimes >= 13 && haveset == 0){
            menu_num = 4;
            bombSound.play();
            startLabel_HARDMODE.visible = true;
            stmeu.kill();
            stmeu = game.add.sprite(0, 540, 'startmenu');
            haveset = 1;
        }
        else if(presstimes < 13 && haveset == 0){
            selectSound.play();
            presstimes++;
            if(menu_num < 3) menu_num++;
            stmeu.kill();
            if(menu_num == 1) stmeu = game.add.sprite(100, 300, 'startmenu');
            if(menu_num == 2) stmeu = game.add.sprite(100, 380, 'startmenu');
            if(menu_num == 3) stmeu = game.add.sprite(100, 460, 'startmenu');
            if(menu_num == 4) stmeu = game.add.sprite(0, 540, 'startmenu');
        }
        else{
            selectSound.play();
            if(menu_num < 4) menu_num++;
            stmeu.kill();
            if(menu_num == 1) stmeu = game.add.sprite(100, 300, 'startmenu');
            if(menu_num == 2) stmeu = game.add.sprite(100, 380, 'startmenu');
            if(menu_num == 3) stmeu = game.add.sprite(100, 460, 'startmenu');
            if(menu_num == 4) stmeu = game.add.sprite(0, 540, 'startmenu');
        }
    },

    enter: function() {
        game.state.add('entername',enternameState);
        game.state.add('play2',play2State);
        game.state.add('play',playState);
        game.state.add('play3',superhardState);
        if(menu_num == 1) {
            game.state.start('play');
        }
        if(menu_num == 2) {
            game.state.start('play2');
        }
        if(menu_num == 3) {
            game.state.start('entername');
        }
        if(menu_num == 4) {
            game.state.start('play3');
        }
    }

}; 