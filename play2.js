var player1;
var player2;

var text4;
var text5;
var text6;

var danger1;
var danger2;

var play2State = {

    preload : function() {

        game.load.baseURL = 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/';
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'player.png', 32, 32);
        game.load.image('wall', 'wall.png');
        game.load.image('ceiling', 'ceiling.png');
        game.load.image('normal', 'normal.png');
        game.load.image('nails', 'nails.png');
    
        game.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'fake.png', 96, 36);
    },
    
    create : function() {

        distance = 0;
        status = 'running';
        platforms = [];
        lastTime = 0;
        danger1 = 0;
        danger2 = 0;
        havebeensetto3 = 0;
    
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
    
        this.createBounders();
        this.createPlayer1();
        this.createPlayer2();
        this.createTextsBoard();

        jumpSound = game.add.audio('jump');
        hurtSound = game.add.audio('hurt');
        ggSound = game.add.audio('gg');
        stepSound = game.add.audio('step');
    },
    
    update : function() {
    
        if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
        if(status != 'running') return;
    
        this.physics.arcade.collide(player1, platforms, this.effect);
        this.physics.arcade.collide(player1, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, platforms, this.effect);
        this.physics.arcade.collide(player2, [leftWall, rightWall]);
        this.physics.arcade.collide(player1, player2, this.playercollide);
        this.physics.arcade.collide(player1, [leftWall2, rightWall2]);
        this.physics.arcade.collide(player2, [leftWall2, rightWall2]);
        this.checkTouchCeiling(player1);
        this.checkTouchCeiling(player2);
        this.checkGameOver();
    
        this.updatePlayer1();
        this.updatePlayer2();
        this.updatePlatforms();
        this.updateTextsBoard();
    
        this.createPlatforms();
    },
    
    createBounders : function() {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;
    
        rightWall = game.add.sprite(483, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 300, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;
    
        rightWall2 = game.add.sprite(483, 300, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;
    
        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling2 = game.add.image(300, 0, 'ceiling');
    },
    
    createPlatforms :　function() {
        if(game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }
    },
    
    createOnePlatform : function() {
    
        var platform;
        var x = Math.random()*(500 - 96 - 40) + 20;
        var y = 600;
        var rand = Math.random() * 100;
    
        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
    
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);
    
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },
    
    createPlayer1 : function() {
        player1 = game.add.sprite(300, 50, 'player');
        player1.direction = 10;
        game.physics.arcade.enable(player1);
        player1.body.gravity.y = 500;
        player1.animations.add('left', [0, 1, 2, 3], 8);
        player1.animations.add('right', [9, 10, 11, 12], 8);
        player1.animations.add('flyleft', [18, 19, 20, 21], 12);
        player1.animations.add('flyright', [27, 28, 29, 30], 12);
        player1.animations.add('fly', [36, 37, 38, 39], 12);
        player1.life = 10;
        player1.unbeatableTime = 0;
        player1.touchOn = undefined;
    },

    createPlayer2 : function() {
        player2 = game.add.sprite(200, 50, 'player');
        player2.direction = 10;
        game.physics.arcade.enable(player2);
        player2.body.gravity.y = 500;
        player2.animations.add('left', [0, 1, 2, 3], 8);
        player2.animations.add('right', [9, 10, 11, 12], 8);
        player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        player2.animations.add('flyright', [27, 28, 29, 30], 12);
        player2.animations.add('fly', [36, 37, 38, 39], 12);
        player2.life = 10;
        player2.unbeatableTime = 0;
        player2.touchOn = undefined;
    },
    
    createTextsBoard : function() {
        var style = {fill: '#1acc4c', fontSize: '20px'}
        text4 = game.add.text(20, 10, '', style);
        text6 = game.add.text(20, 40, '', style);
        text5 = game.add.text(360, 10, '', style);
    },
    
    updatePlayer1 : function() {
        if(keyboard.left.isDown) {
            player1.body.velocity.x = -250;
        } else if(keyboard.right.isDown) {
            player1.body.velocity.x = 250;
        } else {
            player1.body.velocity.x = 0;
        }
        this.setPlayerAnimate(player1);
    },

    updatePlayer2 : function() {
        if(keyboard.a.isDown) {
            player2.body.velocity.x = -250;
        } else if(keyboard.d.isDown) {
            player2.body.velocity.x = 250;
        } else {
            player2.body.velocity.x = 0;
        }
        this.setPlayerAnimate(player2);
    },
    
    setPlayerAnimate : function(player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
    
        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
          player.frame = 8;
        }
    },
    
    updatePlatforms : function() {
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },
    
    updateTextsBoard : function() {
        text4.setText('P1生命:' + player1.life);
        text6.setText('P2生命:' + player2.life);
        text5.setText('目前分數:' + distance);
    },
    
    effect : function(player, platform) {
        if(platform.key == 'conveyorRight') {
            conveyorRightEffect(player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            conveyorLeftEffect(player, platform);
        }
        if(platform.key == 'trampoline') {
            trampolineEffect(player, platform);
        }
        if(platform.key == 'nails') {
            nailsEffect(player, platform);
        }
        if(platform.key == 'normal') {
            basicEffect(player, platform);
        }
        if(platform.key == 'fake') {
            fakeEffect(player, platform);
        }
    },
    
    checkTouchCeiling : function(player) {
        if(player.body.y < 20) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
                player.body.y = 20;
            }
            if(game.time.now > player.unbeatableTime) {
                player.life -= 3;
                hurtSound.play();
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },
    
    checkGameOver :　function() {
        if(danger1 == 0 && (player1.life <= 0 || player1.body.y > 600)) {
            player1.kill();
            danger1 = 1;
            player1.life = 'gg';
        }
        if(danger2 == 0 && (player2.life <= 0 || player2.body.y > 600)) {
            player2.kill();
            danger2 = 1;
            player2.life = 'gg';
        }
        if(danger2 == 1 && (player1.life <= 0 || player1.body.y > 600)) {
            this.gameOver();
        }
        if(danger1 == 1 && (player2.life <= 0 || player2.body.y > 600)) {
            this.gameOver();
        }
    },

    playercollide : function(player1,player2){

    },
    
    gameOver : function () {
        platforms.forEach(function(s) {s.destroy()});
        platforms = [];
        status = 'gameOver';
        ggSound.play();
        var user = firebase.auth().currentUser;
        firebase.database().ref('score').push({
            point: distance,
        }).catch(e => console.log(e.message));
        game.state.add('end',endState);
        game.state.start('end');
    },
    
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    jumpSound.play();
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        hurtSound.play();
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        stepSound.play();
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}
function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        stepSound.play();
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}