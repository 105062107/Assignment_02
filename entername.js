var slickUI;
var name;
var enternameState = {

    preload : function() {
        slickUI = game.plugins.add(Phaser.Plugin.SlickUI);
        game.load.image('backdrop', 'assets/backdrop.png');
        slickUI.load('assets/ui/kenney/kenney.json');
    },

    create : function() {
        var panel;
        slickUI.add(panel = new SlickUI.Element.Panel(8, 8, game.width - 16, game.height - 16));
        panel.add(new SlickUI.Element.Text(10,10, "Press 'B' To Back")).centerHorizontally().text.alpha = 0.5;
        panel.add(new SlickUI.Element.Text(12,34, "Set Up Your Name"));
        var textField = panel.add(new SlickUI.Element.TextField(10,58, panel.width - 20, 40));
        textField.events.onOK.add(function () {
            name = textField.value;
        });
        textField.events.onToggle.add(function (open) {
            console.log('You just ' + (open ? 'opened' : 'closed') + ' the virtual keyboard');
        });
        textField.events.onKeyPress.add(function(key) {
            console.log('You pressed: ' + key);
        });

        var BKey = game.input.keyboard.addKey(Phaser.Keyboard.B);
        BKey.onDown.add(this.B, this);
    },

    B : function(){
        game.state.start('menu');
    }

}