var menu_num = 1;
var stmeu;
var text4;
var highscore1 = 0;
var highscore2 = 0;
var highscore3 = 0;
var name1 = 'no name';
var name2 = 'no name';
var name3 = 'no name';
var endState = {

    preload: function() {
        game.load.image('startmenu', 'assets/start.png');
    },

    create: function() {
    
        stmeu = game.add.sprite(100, 300, 'startmenu');

        var highscore1 = 0;
        var highscore2 = 0;
        var highscore3 = 0;

        var name1 = 'no name';
        var name2 = 'no name';
        var name3 = 'no name';      

        menu_num = 1;

        var nameLabel = game.add.text(game.width/2, 80, 'YOU DIED',
        { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        var startLabel_1P = game.add.text(game.width/2, game.height-280,    
        'PLAY AGAIN 1P', { font: '25px Arial', fill: '#ffffff' });
        startLabel_1P.anchor.setTo(0.5, 0.5);

        var startLabel_2P = game.add.text(game.width/2, game.height-200,    
        'PLAY AGAIN 2P', { font: '25px Arial', fill: '#ffffff' });
        startLabel_2P.anchor.setTo(0.5, 0.5);
    
        var startLabel_QUIT = game.add.text(game.width/2, game.height-120,    
        'QUIT THE GAME', { font: '25px Arial', fill: '#ffffff' });
        startLabel_QUIT.anchor.setTo(0.5, 0.5);

        var startLabel_SCORE = game.add.text(game.width/2, game.height-340,    
        'ur score : ' + distance, { font: '25px Arial', fill: '#ffffff' });
        startLabel_SCORE.anchor.setTo(0.5, 0.5);

        var startLabel_SCORE1 = game.add.text(game.width/2, game.height-440,    
        'highscore1 : ' + highscore1 + 'name: ' + name1, { font: '25px Arial', fill: '#ffffff' });
        startLabel_SCORE1.anchor.setTo(0.5, 0.5);

        var startLabel_SCORE2 = game.add.text(game.width/2, game.height-410,    
        'highscore2 : ' + highscore2 + 'name: ' + name2, { font: '25px Arial', fill: '#ffffff' });
        startLabel_SCORE2.anchor.setTo(0.5, 0.5);

        var startLabel_SCORE3 = game.add.text(game.width/2, game.height-380,    
        'highscore3 : ' + highscore3 + 'name: ' + name3, { font: '25px Arial', fill: '#ffffff' });
        startLabel_SCORE3.anchor.setTo(0.5, 0.5);

        firebase.database().ref('score').once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshat){
                var childData = childSnapshat.val();
                if(childData.point > highscore3){
                    highscore3 = childData.point;
                    name3 = childData.name;
                    if(name3 == null) name3 = 'no name';
                    startLabel_SCORE3.setText('highest score3 : ' + highscore3 + ' name: ' + name3);
                }
                if(highscore3 > highscore2){
                    var temp1;
                    temp1 = highscore3;
                    highscore3 = highscore2;
                    highscore2 = temp1;
                    var nametemp1;
                    nametemp1 = name3;
                    name3 = name2;
                    name2 = nametemp1;
                    if(name3 == null) name3 = 'no name';
                    if(name2 == null) name2 = 'no name';
                    startLabel_SCORE2.setText('highest score2 : ' + highscore2 + ' name: ' + name2);
                    startLabel_SCORE3.setText('highest score3 : ' + highscore3 + ' name: ' + name3);
                }
                if(highscore2 > highscore1){
                    var temp2;
                    temp2 = highscore2;
                    highscore2 = highscore1;
                    highscore1 = temp2;
                    var nametemp2;
                    nametemp2 = name2;
                    name2 = name1;
                    name1 = nametemp2;
                    if(name1 == null) name1 = 'no name';
                    if(name2 == null) name2 = 'no name';
                    startLabel_SCORE2.setText('highest score2 : ' + highscore2 + ' name: ' + name2);
                    startLabel_SCORE1.setText('highest score1 : ' + highscore1 + ' name: ' + name1);
                }
            })
        })
        .catch(e => console.log(e.message));

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        upKey.onDown.add(this.up, this);
        downKey.onDown.add(this.down, this);
        enterKey.onDown.add(this.enter, this);
        
    },

    up: function() {
        if(menu_num != 1) menu_num--;
        stmeu.kill();
        if(menu_num == 1) stmeu = game.add.sprite(100, 300, 'startmenu');
        if(menu_num == 2) stmeu = game.add.sprite(100, 380, 'startmenu');
        if(menu_num == 3) stmeu = game.add.sprite(100, 460, 'startmenu');
    },

    down: function() {
        if(menu_num != 3) menu_num++;
        stmeu.kill();
        if(menu_num == 1) stmeu = game.add.sprite(100, 300, 'startmenu');
        if(menu_num == 2) stmeu = game.add.sprite(100, 380, 'startmenu');
        if(menu_num == 3) stmeu = game.add.sprite(100, 460, 'startmenu');
    },

    enter: function() {
        game.state.add('play',playState);
        if(menu_num == 1) game.state.start('play');
        if(menu_num == 2) game.state.start('play2');
        if(menu_num == 3) game.state.start('menu');
    },

}; 