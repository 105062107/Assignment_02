var hardmodestage = 1;
var havebeensetto3 = 0;
var havebeensetto4 = 0;
var superhardState = {

    preload : function() {

        game.load.baseURL = 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/';
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'player.png', 32, 32);
        game.load.image('wall', 'wall.png');
        game.load.image('ceiling', 'ceiling.png');
        game.load.image('normal', 'normal.png');
        game.load.image('nails', 'nails.png');
    
        game.load.spritesheet('conveyorRight', 'conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'fake.png', 96, 36);
    },
    
    create : function() {

        distance = 0;
        status = 'running';
        platforms = [];
        lastTime = 0;
        hardmodestage = 1;
        havebeensetto3 = 0;
        havebeensetto4 = 0;
    
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });

        jumpSound = game.add.audio('jump');
        hurtSound = game.add.audio('hurt');
        ggSound = game.add.audio('gg');
        stepSound = game.add.audio('step');

        button_left = game.add.sprite(20, 500, 'buttonleft');
        button_left.events.onInputDown.add(this.left, this);
        button_left.inputEnabled = true;
        button_left.input.useHandCursor = true;

        button_right = game.add.sprite(320, 500, 'buttonright');
        button_right.events.onInputDown.add(this.right, this);
        button_right.inputEnabled = true;
        button_right.input.useHandCursor = true;

        button_up = game.add.sprite(170, 500, 'buttonup');
        button_up.events.onInputDown.add(this.up, this);
        button_up.inputEnabled = true;
        button_up.input.useHandCursor = true;

        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();

        /*button_right = game.add.button(20  , 10, 'normal', actionOnClickright, this, 2, 1, 0);
        button_left  = game.add.button(380 , 10, 'normal' , actionOnClickleft , this, 2, 1, 0);
        button_right.onInputOver.add(over, this);
        button_right.onInputOut.add(out, this);
        button_right.onInputUp.add(up, this);
        button_left.onInputOver.add(over, this);
        button_left.onInputOut.add(out, this);
        button_left.onInputUp.add(up, this);*/
    },
    
    update : function() {
    
        if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
        if(status != 'running') return;
    
        this.physics.arcade.collide(player, platforms, this.effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player, [leftWall2, rightWall2]);
        this.checkTouchCeiling(player);
        this.checkGameOver();
    
        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();
    
        this.createPlatforms();
        this.updatestage();
        this.ifstage3();
        this.ifstage4();
    },
    
    createBounders : function() {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;
    
        rightWall = game.add.sprite(483, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 300, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;
    
        rightWall2 = game.add.sprite(483, 300, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;
    
        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling2 = game.add.image(300, 0, 'ceiling');
    },
    
    createPlatforms :　function() {
        if(distance > 145 && distance <205){
            if(game.time.now > lastTime + 900) {
                lastTime = game.time.now;
                this.createOnePlatform();
                distance += 1;
            }
        }
        else{
            if(game.time.now > lastTime + 600) {
                lastTime = game.time.now;
                this.createOnePlatform();
                distance += 1;
            } 
        }
    },
    
    createOnePlatform : function() {
    
        var platform;
        var x = Math.random()*(500 - 96 - 40) + 20;
        var y = 600;
        var rand = Math.random() * 100;
    
        if(hardmodestage == 1) {
            platform = game.add.sprite(x, y, 'normal');
        } else if(hardmodestage == 2){
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        } else if(hardmodestage == 3){
            if(rand < 30) {
                platform = game.add.sprite(x, y, 'normal');
            } else if (rand < 53) {
                platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(platform);
                platform.body.setSize(96, 15, 0, 15);
            } else if (rand < 60) {
                platform = game.add.sprite(x, y, 'conveyorLeft');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 67) {
                platform = game.add.sprite(x, y, 'conveyorRight');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 85) {
                platform = game.add.sprite(x, y, 'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            } else {
                platform = game.add.sprite(x, y, 'fake');
                platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
        } else if(hardmodestage == 4){
            if(rand < 50) {
                platform = game.add.sprite(x, y, 'normal');
            } else{
                platform = game.add.sprite(x, y, 'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            }
        } else if(hardmodestage == 5){
            if (rand < 25) {
                platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(platform);
                platform.body.setSize(96, 15, 0, 15);
            } else if (rand < 40) {
                platform = game.add.sprite(x, y, 'conveyorLeft');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 55) {
                platform = game.add.sprite(x, y, 'conveyorRight');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 75) {
                platform = game.add.sprite(x, y, 'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            } else {
                platform = game.add.sprite(x, y, 'fake');
                platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
        }else{
            if(rand < 20) {
                platform = game.add.sprite(x, y, 'normal');
            } else if (rand < 40) {
                platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(platform);
                platform.body.setSize(96, 15, 0, 15);
            } else if (rand < 55) {
                platform = game.add.sprite(x, y, 'conveyorLeft');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 70) {
                platform = game.add.sprite(x, y, 'conveyorRight');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            } else if (rand < 85) {
                platform = game.add.sprite(x, y, 'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            } else {
                platform = game.add.sprite(x, y, 'fake');
                platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
        }
    
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);
    
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },
    
    createPlayer : function() {
        player = game.add.sprite(200, 50, 'player');
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.life = 10;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
    },
    
    createTextsBoard : function() {
        var style = {fill: '#1acc4c', fontSize: '20px'}
        text1 = game.add.text(20, 10, '', style);
        text2 = game.add.text(360, 10, '', style);
    },
    
    updatePlayer : function() {
        if(distance<205){
            if(keyboard.left.isDown) {
                player.body.velocity.x = -250;
            } else if(keyboard.right.isDown) {
                player.body.velocity.x = 250;
            } else {
                player.body.velocity.x = 0;
            }
            this.setPlayerAnimate(player);
        }
       else{
        if(keyboard.left.isDown) {
            player.body.velocity.x = -400;
        } else if(keyboard.right.isDown) {
            player.body.velocity.x = 400;
        } else {
            player.body.velocity.x = 0;
        }
        this.setPlayerAnimate(player);
       }
    },
    
    setPlayerAnimate : function(player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
    
        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
          player.frame = 8;
        }
    },
    
    updatePlatforms : function() {
        if(distance < 205){
            for(var i=0; i<platforms.length; i++) {
                var platform = platforms[i];
                platform.body.position.y -= 2;
                if(platform.body.position.y <= -20) {
                    platform.destroy();
                    platforms.splice(i, 1);
                }
            }
        }
        else{
            for(var i=0; i<platforms.length; i++) {
                var platform = platforms[i];
                platform.body.position.y -= 4;
                if(platform.body.position.y <= -20) {
                    platform.destroy();
                    platforms.splice(i, 1);
                }
            }
        }
    },
    
    updateTextsBoard : function() {
        if(name == null)    text1.setText('玩家生命:' + player.life);
        else text1.setText(name + '生命:' + player.life);
        if(distance <= 25) text2.setText('階級:1');
        else if(distance <= 40) text2.setText('階級:2');
        else if(distance <= 95) text2.setText('階級:3');
        else if(distance <= 145) text2.setText('階級:4');
        else if(distance <= 205) text2.setText('階級:5');
        else text2.setText('階級:6');
    },
    
    effect : function(player, platform) {
        if(platform.key == 'conveyorRight') {
            conveyorRightEffect(player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            conveyorLeftEffect(player, platform);
        }
        if(platform.key == 'trampoline') {
            trampolineEffect(player, platform);
        }
        if(platform.key == 'nails') {
            nailsEffect(player, platform);
        }
        if(platform.key == 'normal') {
            basicEffect(player, platform);
        }
        if(platform.key == 'fake') {
            fakeEffect(player, platform);
        }
    },
    
    checkTouchCeiling : function(player) {
        if(player.body.y < 20) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
                player.body.y = 20;
            }
            if(game.time.now > player.unbeatableTime) {
                player.life -= 3;
                hurtSound.play();
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },
    
    checkGameOver :　function() {
        if(player.life <= 0 || player.body.y > 600) {
            this.gameOver();
        }
    },

    actionOnClickright : function(){
        player.body.velocity.x = -250;
    },

    actionOnClickleft : function(){
        player.body.velocity.x = 250;
    },
    
    gameOver : function () {
        platforms.forEach(function(s) {s.destroy()});
        platforms = [];
        status = 'gameOver';
        ggSound.play();
        var user = firebase.auth().currentUser;
        firebase.database().ref('score').push({
            point: distance,
            name: name,
        }).catch(e => console.log(e.message));
        game.state.add('end',endState);
        game.state.start('end');
    },
    
    left : function(){
        player.body.velocity.x = -1550;
    },

    right : function(){
        player.body.velocity.x = 1550;
    },

    up : function(){
        player.body.velocity.y = -250;
    },

    updatestage : function(){
        if(distance <= 20) hardmodestage = 1;
        else if(distance <= 35) hardmodestage = 2;
        else if(distance <= 90) hardmodestage = 3;
        else if(distance <= 140) hardmodestage = 4;
        else if(distance <= 200) hardmodestage = 5;
        else hardmodestage = 6;
    },

    ifstage3 : function(){
        if(distance > 40 && havebeensetto3 == 0){
            havebeensetto3 = 1;
            if(player.life > 4)player.life = 4;
        }
    },

    ifstage4 : function(){
        if(distance > 95 && havebeensetto4 == 0){
            havebeensetto4 = 1;
            player.life = 10;
        }
    },
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    jumpSound.play();
    if(distance > 95 && distance < 145 && havebeensetto3 == 1){
        player.body.velocity.y = -500;
    }
    else player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        hurtSound.play();
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        stepSound.play();
        if(hardmodestage!=3 && player.life < 10) {
            player.life += 1;
        }
        else if(hardmodestage == 3 && player.life < 4){
            player.life += 1;
        }
        else{
        }
        player.touchOn = platform;
    }
}
function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        stepSound.play();
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}





